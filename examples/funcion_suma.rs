fn suma(a: i32, b: i32) -> i32 {
    let c = a + b;
    return c;
}

fn main() {
    let a = 9;
    let b = 15;
    let c = suma(a, b);
    println!("La suma de {} + {} es: {}", a, b, c);
}
