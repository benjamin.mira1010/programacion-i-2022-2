use std::io::stdin;

fn main(){
    println!("Ingrese un numero");
    let mut entrada: String = String::new();
    stdin().read_line(&mut entrada).unwrap();

    let numero:u32 = entrada
        .trim()
        .parse()
        .expect("no es un numero");

    println!("El numero ingresado es {}:", numero);
}
