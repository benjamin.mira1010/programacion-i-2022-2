use std::io::stdin;

fn main(){
    loop{
        println!("Ingrese un numero");
        let mut entrada: String = String::new();
        stdin().read_line(&mut entrada).unwrap();
        let numero: i32 = match entrada.trim().parse(){
            Ok(algo) => algo,
            Err(_) => {
                println!("No es un número válido"); 
                continue
            },
        };
    println!("El numero es: {}", numero);
    break;
    }
}
