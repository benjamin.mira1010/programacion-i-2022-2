fn suma(a: i32, b: i32) -> () {
    let c = a + b;
    println!("La suma de {} + {} es: {}", a, b, c);
}

fn main() {
    let a = 9;
    let b = 15;
    suma(a, b);
}
