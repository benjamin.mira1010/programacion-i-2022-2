use std::io::stdin;

fn main(){
    println!("Ingrese un numero");
    let mut entrada: String = String::new();
    stdin().read_line(&mut entrada);
    //stdin().read_line(&mut entrada).unwrap();

    let numero:u32 = entrada
        .trim()
        .parse()
        .unwrap();

    println!("El numero ingresado es {}:", numero);
}
