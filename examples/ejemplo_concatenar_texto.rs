fn main() {

    let txt_a: &str = "Hola";
    let txt_b: &str = "Mundo";

    let concat = format!("{} {}", txt_a, txt_b);

    println!("{}", concat);

    // String +  &str
    
    let mut string_a: String = "Hola ".to_owned();
    let txt_b: &str = "Mundo";

    string_a.push_str(txt_b);
    println!("{}", string_a);


    // String +  &str
    
    let mut string_a: String = "Hola ".to_string();
    let txt_b: &str = "Mundo";

    string_a.push_str(txt_b);
    println!("{}", string_a);


    // String + String
    
    let mut string_a: String = "Hola ".to_string();
    let string_b: String = "Mundo".to_string();

    string_a.push_str(&string_b);
    println!("{}", string_a);


    // String + String
    
    let string_a: String = "Hola".to_string();
    let string_b: String = "Mundo".to_string();

    let string_c = format!("{} {}", string_a, string_b);
    println!("{}", string_c);

}
