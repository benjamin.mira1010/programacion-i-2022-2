use std::io::stdin;

fn main() {
    println!("Ingrese su edad:");
    let mut edad: String = String::new();
    stdin().read_line(&mut edad).unwrap();
    let edad_int: i8 = edad.trim().parse().unwrap();

    println!("La edad es: {}", edad_int);
}
