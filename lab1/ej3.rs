fn main(){
    let mut a = 4;
    let mut b = 3;
    let mut mayor = 0;

    while (a > 4) && (b<100){
        println!("{}", a);
        if a > b {
            mayor = a;
        } else if b > a {
            mayor = b;
        } else {
            mayor = 0 ;
        }
        a = a + mayor;
        b = b * mayor;
    }

    println!("{} {}", a, b);

}
