fn main(){

    let mut ahorro: u16 = 0;
    let mut dinero_ahorrado: u16 = 10000;

    // doce meses, comienza en 1
    for mes in 1..13 {
        ahorro = 1000;
        dinero_ahorrado = ahorro;
        println!("En el mes {} lleva ahorrado {}", mes, dinero_ahorrado);
    }

println!("El total de dinero ahorrado es: {}", dinero_ahorrado);

}
