fn main() {
    let mut a = 10;
    let mut b;

    while a > 0 {
        b = a - 1;
        print!("{},", b);
        a = a - 1;
    }
    println!("");
}
