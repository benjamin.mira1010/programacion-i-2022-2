use std::io::stdin;

fn main(){

    println!("Ingrese una oración: ");
    let mut oracion: String = String::new();
    stdin().read_line(&mut oracion).unwrap();

    println!("Ingrese una palabra: ");
    let mut palabra: String = String::new();
    stdin().read_line(&mut palabra).unwrap();

    if oracion.contains(&palabra.trim()){

        println!("La palabra existe en la oracion");
    } else {

        println!("La palabra no existe en la oracion");
    }

}
