use std::io::stdin;

fn main() {
    let mut word: String = String::new();
    stdin().read_line(&mut word).unwrap();
    println!("{}", &word.trim());
    if is_a_color_word(&word.trim()) {
        println!("That is a color word I know!");
    } else {
        println!("That is not a color word I know.");
    }
}

fn is_a_color_word(attempt: &str) -> bool {
    attempt == "green" || attempt == "blue" || attempt == "red"
}
