use std::io::stdin;

fn reverse(input: &str) -> String {
    let mut output = String::new();
    for c in input.to_string().chars() {
        output = c.to_string() + &output;
    }
    return output
}


fn main() {
    let mut word: String = String::new();
    stdin().read_line(&mut word).unwrap();
    let word = reverse(&word);
    println!("{}", word);
}
