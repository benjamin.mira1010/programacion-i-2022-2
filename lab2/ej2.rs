fn is_armstrong_number(num: u32) -> bool {
    let mut sum = 0;
    let number = num.to_string();
    for digit in number.chars() {
        //let digit = digit as u32;
        let digit = digit as u32 - '0' as u32;
        sum += digit.pow(number.len() as u32);
    }
    
    if num == sum {
        return true
    }else{
        return false
    }
    //return sum;
}

fn main(){
    
    let mut contador = 0;
    loop{
        if is_armstrong_number(contador) {
            println!("Si, es un numero Armstrong");
        } else {
            println!("No, no lo es");
        }
        contador = contador + 1;
        if contador == 1000{
            break;
        }
    }


}
